# __Voyelles – Arthur Rimbaud__ 

A noir, E blanc, I rouge, U vert, O bleu: voyelles,
Je dirai quelque jour vos naissances latentes:
A, noir corset velu des mouches éclatantes
Qui bombinent autour des puanteurs cruelles,

Golfes d’ombre; E, candeurs des vapeurs et des tentes,
Lances des glaciers fiers, rois blancs , frissons d’ombelles;
I, pourpres, sang craché, rire des lèvres belles
Dans la colère ou les ivresses pénitentes;

U, cycles, vibrements divins des mers virides,
Paix des pâtis semés d’animaux, paix des rides
Que l’alchimie imprime aux grands fronts studieux;

O, suprême Clairon plein des strideurs étranges,
Silences traversés des Mondes et des Anges:
– Ô l’Oméga, rayon violet de Ses Yeux!

da “Œuvres complètes”, a cura di Antoine Adam, “Bibliothèque de la Pléiade”, Paris, 1972

∗∗∗

## Vocali

A nera, E bianca, I rossa, U verde, O blu: vocali,
Io dirò un giorno le vostre nascite latenti:
A, nero corsetto, vello di mosche splendenti,
Ronzanti intorno a crudeli fetori, golfi

D’ombra; E, candore di vapori e tende, lance
Di ghiacciai fieri, bianchi re, frementi umbelle;
I, porpore, sputo di sangue, labbra belle
In riso di collera o d’ebbrezze penitenti;

U, cicli, vibramenti divini dei viridi mari,
Pace d’animali al pascolo, pace di rughe
Impresse dall’alchimia su ampie fronti studiose;

O, Tromba suprema piena d’arcani stridori,
Silenzi attraversati dagli Angeli e dai Mondi:
– Oh l’Omega, di quei Suoi Occhi il raggio viola!

Arthur Rimbaud

(Traduzione di Diana Grange Fiori)

da “Arthur Rimbaud, Opere”, “I Meridiani” Mondadori, 1975

∗∗∗

## Vocali

A nera, E bianca, I rossa, U verde, O blu: vocali,
Io dirò un giorno i vostri ascosi nascimenti:
A, nero vello al corpo delle mosche lucenti
Che ronzano al di sopra dei crudeli fetori,

Golfi d’ombra; E, candori di vapori e di tende,
Lance di ghiaccio, brividi di umbelle, bianchi re;
I, porpore, rigurgito di sangue, labbra belle
Che ridono di collera, di ebbrezza penitente;

U, cicli, vibrazioni sacre dei mari viridi,
Quiete di bestie al pascolo, quiete dell’ampie rughe
Che alle fronti studiose imprime l’alchimia.

O, la suprema Tuba piena di stridi strani,
Silenzi attraversati dagli Angeli e dai Mondi:
— O, l’Omega ed il raggio violetto dei Suoi Occhi!

Arthur Rimbaud

(Traduzione di Ivos Margoni)

da “Arthur Rimbaud, Opere”, Feltrinelli, Milano, 1964

∗∗∗

## Vocali

A nera, E bianca, I rossa, U verde, O blu: vocali,
io dirò un giorno le vostre segrete origini:
A, nero, corsetto villoso delle mosche lucenti
che ronzano intorno a crudeli fetori,

golfi d’ombra; E, candori di vapori e di tende,
lance di fieri ghiacciai, bianchi re, brividi d’umbelle;
I, porpora, sputo di sangue, riso di belle labbra
nella collera o nelle ebrezze penitenti;

U, cicli, fremiti divini di mari verdi,
pace dei pascoli disseminati di animali, pace delle rughe
che l’alchimia scava nelle ampie fronti studiose;

O, Tromba suprema piena di stridori strani,
silenzi solcati dai Pianeti e dagli Angeli:
– O l’Omega e il raggio violetto dei Suoi Occhi!

Arthur Rimbaud

(Traduzione di Laura Mazza)

da “Arthur Rimbaud, Tutte le poesie”, Newton Compton, 1972

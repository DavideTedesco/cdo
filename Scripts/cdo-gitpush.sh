#!/bin/sh

#  ALEVO-gitpush.sh
#
# use it by terminal typing:
# bash cdo-gitpush.sh

cd /Users/davide/gitlab/DavideTedesco/cdo

git add .
DATE=$(date)
git commit -am "changes made on $DATE"
git push

primorigo = \new Staff \with{
         
             instrumentName = "Flauto Basso"
        shortInstrumentName = "Fl. B."
                midiInstrument = "flute"
\consists "Page_turn_engraver" 

        
    }    {
      \primavoce}
    
secondorigo =    \new Staff \with{
        instrumentName = "Voce Maschile"
        shortInstrumentName = "V. M."
        midiInstrument = "choir aahs"
        \consists "Page_turn_engraver" 
    \clef "bass"
    \new Voice{
        }
    
}{\secondavoce} 

terzorigo =    \new Staff \with{
        instrumentName = "Elettronica sul Palco"
        shortInstrumentName = "E. P."
                midiInstrument = "choir aahs"
                   \hide Clef
                         %\remove "Time_signature_engraver"
                \consists "Page_turn_engraver" 
                 
\new Voice{
        }
}{\terzavoce}
      
    
 quartorigo =  \new Staff \with{
        instrumentName = "Elettronica in Sala"
  shortInstrumentName = "E. S."
          midiInstrument = "choir aahs"
          %\hide Clef
           %\remove "Time_signature_engraver"
\consists "Page_turn_engraver" 
 
   \new Voice{
        }
 }{\quartavoce}
%===================SEPARATORE
%slashseparator=========================
  bigSlashSeparator = \markup {
  \center-align
\vcenter
\combine
\beam #5.0 #0.5 #0.48
\raise #1
\beam #5.0 #0.5 #0.48
 }
\paper {
%             #:music "ross"

                #(define fonts
                  (set-global-fonts	
                   #:roman "IMFellEnglish"
                   #:music "ross"
                   #:brace "ross"
                   #:factor (/ staff-height pt 20)
                  ))
		%page number customization
                print-page-number = ##t
                print-first-page-number = ##t
                %centering page numbers
                oddHeaderMarkup = \markup \null
    		evenHeaderMarkup = \markup \null
    		evenHeaderMarkup = \markup \null
    		oddFooterMarkup = \markup {
     		 \fill-line {
       		 \on-the-fly \print-page-number-check-first
       		 \fromproperty #'page:page-number-string
     		 }
    	  	 }

                %system-separator-markup = \slashSeparator
                %custom slash separator
                system-separator-markup = \bigSlashSeparator
		 #(set-paper-size "a4"'portrait )
 			 top-margin = 1.5\cm
  			after-title-space = 10\mm
  			bottom-margin = 2\cm
  			indent = 15\mm
 		line-width = 180\mm - 3.0 * 10.16\mm
 		
  %page-count = #2
    system-system-spacing = #'((basic-distance . 0.5) (padding . 10))
  page-breaking = #ly:page-turn-breaking
  
  %systems-per-page= #2


  % spacing variables may be changed individually:
  %system-system-spacing.padding = #8
  % or as a group:
  system-system-spacing =
    #'((basic-distance . 12)
       (minimum-distance . 10)
       (padding . 1)
       (stretchability . 70)) 


}
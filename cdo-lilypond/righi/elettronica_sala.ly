lengthen-gliss = 
#(define-music-function (nmbr)(number?)
#{
  \once \override Glissando.springs-and-rods = #ly:spanner::set-spacing-rods
  \once \override Glissando.minimum-length = #nmbr
#})


                            % (cons (* (car e) factor) (define-sine-function (car e))))
  


fancy-gliss =
#(define-music-function (pts-list)(list?)
   
#{
 \once \override Glissando.after-line-breaking =
 
  #(lambda (grob)
    (let ((stil (ly:line-spanner::print grob)))
     (if (ly:stencil? stil)
         (let* 
           ((left-bound-info (ly:grob-property grob 'left-bound-info))
            (left-bound (ly:spanner-bound grob LEFT))
            (y-off (assoc-get 'Y left-bound-info))
            (padding (assoc-get 'padding left-bound-info))
            (note-column (ly:grob-parent left-bound X))
            (note-heads (ly:grob-object note-column 'note-heads))
            (ext-X 
              (if (null? note-heads)
                  '(0 . 0)
                  (ly:relative-group-extent note-heads grob X)))
            (dot-column (ly:note-column-dot-column note-column))
            (dots 
              (if (null? dot-column)
                  '()
                  (ly:grob-object dot-column 'dots)))
            (dots-ext-X 
              (if (null? dots)
                  '(0 . 0)
                  (ly:relative-group-extent dots grob X)))
            (factor 
              (/ (interval-length (ly:stencil-extent stil X))
                 (car (take-right (last pts-list) 2))))
            (new-stil
              (make-connected-path-stencil 
                (map
                  (lambda (e)
                    (cond ((= (length e) 2)
                           (cons (* (car e) factor) (cdr e)))
                          ((= (length e) 6)
                           (list
                             (* (car e) factor)
                             (cadr e)
                             (* (third e) factor)
                             (fourth e)
                             (* (fifth e) factor)
                             (sixth e)
                            ;; (* (seventh e) factor)
                            ;; (eighth e)
                             ))
                          (else 
                            (ly:error 
                              "Some element(s) of the given list do not fit"))))   
                  pts-list)
                (layout-line-thickness grob )  ;line-width                     
                1   ;scaling
                1   ;scaling
                #f
                #f)))
         (ly:grob-set-property! grob 'stencil
           (ly:stencil-translate
            new-stil
            (cons (+ (interval-length ext-X) 
                     (interval-length dots-ext-X) 
                     padding) 
                  y-off))))
       (begin
         (ly:warning 
           "Cannot find stencil. Please set 'minimum-length accordingly")
         #f))))
#})


quartavoce = 
 \relative c''
 
{
  \once \hide Staff.Clef
   \override Staff.StaffSymbol.line-count = #1  s1*7/4^\aton 
   \stopStaff
          \clef bass

    \startStaff
        \once \override Staff.StaffSymbol.line-count = #5 
          <<{dis,,1^\tapefl^\markup{\box "1"}
          \glissando e2.} {s4\p\< s4 s4 s4 s4 s4 s4\f}>>
           \override Glissando.cross-staff = ##t
                % \lengthen-gliss #7
     \fancy-gliss
        #'(
         (0 0 
           0.7 -2
           1.8 1
           ;;0.8 -0.5
          )

         )
        
          <<{dis1*6/4^\tapefl^\markup{\box "2"}%\glissando %mettere apposto il glissando sinusoidale
            }{s4\p\< s4 s4 s4 s4 s4 s4\f}>>
          %-\markup
%{\translate #'(0 . 0) 
               % \override #'(height . 1)
            %\draw-squiggle-line #0.5 #'(12 . 0) ##t
               

%}
          %\once \hideNotes dis4\f
          
         % e\glissando b
          

      
}
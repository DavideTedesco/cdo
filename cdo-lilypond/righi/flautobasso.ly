bins = \markup {
            \column {
    \line {\rounded-box "Buio in sala"}
            }
}

lsp = \markup {
            \column { %\translate #'(+5 . 3)
    \line {\rounded-box "Luce graduale sui pannelli"}
    
            }
}

erre = \markup{\column{
                                      \line{Facendo entrare la}
                                      \line{ R progressivamente}
               }			
               }
               
manill = \markup {\rounded-box 
            \column {
    \line {La mano }
    \line{viene illuminata}
    \line{velocemente}
}}

manluoff = \markup {\rounded-box 
            \column {
    \line {Luce mano }
    \line{viene spenta}
    \line{lentamente}
}}

lucvecon = \markup {\rounded-box 
            \column {
    \line {Luce accesa }
    \line{velocemente}
    \line{che illumina}
    \line{tutto il palco}
}}
tongueram = \markup{\huge "Tongue Ram"}%https://www.flutexpansions.com/tongue-ram
tonguerampizz = \markup{\huge \column{\line{Tongue Ram} \line{Pizzicato}}}%https://www.flutexpansions.com/tongue-ram
              
tram = \tweak style #'triangle \etc %https://lsr.di.unimi.it/LSR/Item?id=485

fancy-gliss = %https://lsr.di.unimi.it/LSR/Item?id=1066
#(define-music-function (pts-list)(list?)
   
#{
 \once \override Glissando.after-line-breaking =
 
  #(lambda (grob)
    (let ((stil (ly:line-spanner::print grob)))
     (if (ly:stencil? stil)
         (let* 
           ((left-bound-info (ly:grob-property grob 'left-bound-info))
            (left-bound (ly:spanner-bound grob LEFT))
            (y-off (assoc-get 'Y left-bound-info))
            (padding (assoc-get 'padding left-bound-info))
            (note-column (ly:grob-parent left-bound X))
            (note-heads (ly:grob-object note-column 'note-heads))
            (ext-X 
              (if (null? note-heads)
                  '(0 . 0)
                  (ly:relative-group-extent note-heads grob X)))
            (dot-column (ly:note-column-dot-column note-column))
            (dots 
              (if (null? dot-column)
                  '()
                  (ly:grob-object dot-column 'dots)))
            (dots-ext-X 
              (if (null? dots)
                  '(0 . 0)
                  (ly:relative-group-extent dots grob X)))
            (factor 
              (/ (interval-length (ly:stencil-extent stil X))
                 (car (take-right (last pts-list) 2))))
            (new-stil
              (make-connected-path-stencil 
                (map
                  (lambda (e)
                    (cond ((= (length e) 2)
                           (cons (* (car e) factor) (cdr e)))
                          ((= (length e) 6)
                           (list
                             (* (car e) factor)
                             (cadr e)
                             (* (third e) factor)
                             (fourth e)
                             (* (fifth e) factor)
                             (sixth e)
                            ;; (* (seventh e) factor)
                            ;; (eighth e)
                             ))
                          (else 
                            (ly:error 
                              "Some element(s) of the given list do not fit"))))   
                  pts-list)
                (layout-line-thickness grob )  ;line-width                     
                1   ;scaling
                1   ;scaling
                #f
                #f)))
         (ly:grob-set-property! grob 'stencil
           (ly:stencil-translate
            new-stil
            (cons (+ (interval-length ext-X) 
                     (interval-length dots-ext-X) 
                     padding) 
                  y-off))))
       (begin
         (ly:warning 
           "Cannot find stencil. Please set 'minimum-length accordingly")
         #f))))
#})
%====================

primavoce = \relative c''
{   
  \time 7/4 
   \tempo 4 = 85
   cis1.^\bins \ppp r4  
   cis2.\p \breathe d4^>\sfz d2.
   (d2.)  f,2^\markup{\circle"1"} r2
   \time 6/4 
   R1*6/4^\lsp 
   r1 r4 <<{dis4 
   (dis1)} {s4\f\> s4 s4 s4 s4\p\!}>> \breathe  r2
   gis1.\f \time 4/4 
   r1 
   cis,8\p\< (e2 bes4.)\f^\markup{\circle"2"} 
   r1 
   r1 
   r1
   \clef bass 
  <<{fis1:32^\erre} {s4\p\<  s4 s4 s4\ff\!}>> 
  r4. <<{g8:64 (g2:32) 
  (g1:32)} \alniente  {s8\f\> s4 s4 s4 s4 s4 s4\!}>>
   \time 6/4
   \clef treble 
   r1  g'2
   \clef bass 
   g,2 r4 r4 g4.:32 r8
   g2.\f a2.\mf
   r2. f2.\p^\rallentando
   (f4) cis2.\f^\markup{\circle"3"}  r2
   R1*6/4
   \tempo 4 = 40
   R1*6/4
   R1*6/4
   R1*6/4
   \time 4/4
   R1*4/4
   R1*4/4
   <<{R1*4/4} {s4 s4 s4^\bins s4}>>
   %fine A
  \clef treble
  \tempo 4=70
  <<{f''1:32^\erre (f2)}{s4\pp\< s4 s4 s4^\markup{\circle "5"} s4 s4\f}>> r2^\manill
   r1^\rallentando
   r1^\manluoff
   \tempo 4=60
   r1^\markup{\box{\column{
                                              \line{Il flautista mostra}
                                                      \line{lo strumento} 
                                                      \line{uscendo dal telo}
                                                      \line{rimanendo in posizione}
                                                      \line{senza mostrare il volto}
                        }
             }}^\markup{\italic {inserire volto flautista a sx del telo}}%modificare
    \clef bass
     a,,4.^>\f^\lucvecon r8 c,16\mf r8. r4
     \clef treble

     <f'\ff \tram f,>4\f^\tongueram r4 r4 <f\tram f,>
     (<f \tram f,>8) r8 
     \fancy-gliss
        #'(
         (0 0 0.6 -0.5 1 0.5)
         
         )
     f2.\mf\<\glissando^\markup{\column{
                     \line{Cresce girando l'imboccatura}
                     \line{da tutta chiusa a tutta aperta}
                           
     }}
     g4\f r2.
     <<{e'1}{s4\p\<
              s4 s4 s4\f}>>%^\markup{Cresce normalmente} 
     \time 6/4
     \clef treble
     g,1.\p^\markup{Crescendo subito fino a \dynamic f}
     r1.
     \tempo 4=90
     b2.\mf\< (c2.)
     (c4\f) r1 r4
     \clef bass 
     dis,,1.\f
     r1 bes2
     (bes2) r8 dis2 (f4.)
     (f8) e8 dis8 c8 \breathe e2. fis4
     (fis1) <c\ff \tram c,>16\f^\tonguerampizz\staccato  <c\tram c,>16\staccato \clef treble <<{f''16^\markup{\column{\line{Suonando} \line{normalmente}}} c16 cis8 d8
     (d2)} {\alniente s16\f\> s16 s8 s8  s4 s4\!}>> \clef bass <<{b,,1} {\alniente s4\f\>^\markup{chiudendo l'imboccatura} s4 s4 s8 s8\!}>> \breathe
     <<{e1.:32} {\alniente s4\mf\> s4 s4 s4 s4 s4\!}>>
}
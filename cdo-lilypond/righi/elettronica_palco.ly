frfl = \markup{Freeze flauto \circle "1"}
triangoloup = 
\markup \stencil 
#(make-connected-path-stencil
  '((1 2)  ;; path coordinates
    (2 0)
    (0 0))
  0.3 ;; line thickness
  1  ;; X-axis scaling factor
  1  ;; Y-axis scaling factor
  #f  ;; auto-connect path to origin point? (0 0)
  #t)  % filled path?

tristencil =
#(ly:make-stencil (list 'embedded-ps
    "gsave
      currentpoint translate
      newpath
      0 -0.2 moveto
      1 1.8 lineto
      2 -0.2 lineto
      0 -0.2 lineto
      closepath
      fill
      grestore" )
    (cons 0 1.3125)
    (cons -.75 3))

tristencildown =
#(ly:make-stencil (list 'embedded-ps
    "gsave
      currentpoint translate
      newpath
      0.2 0.2 moveto
      0.2 0.2 lineto
      2.2 0.2 lineto
      1.2 -1.8 lineto
      closepath
      fill
      grestore" )
    (cons 0 1.3125)
    (cons -.75 3))

triangolodown = 
\markup \stencil 
#(make-connected-path-stencil
  '((1 2)  ;; path coordinates
    (2 0)
    (0 0))
  0.15  ;; line thickness
  1  ;; X-axis scaling factor
  -1  ;; Y-axis scaling factor
  #f  ;; auto-connect path to origin point? (0 0)
  #t)  % filled path?

%======TRIANGLENOTE

triangolouponce = {
  \once \override Dots.dot-count = #0 %https://lilypond.org/doc/v2.21/Documentation/notation/writing-rhythms
   \once            \hide Stem
\once\override NoteHead #'no-ledgers = ##t
  \once\override Accidental #'stencil = ##f
  \once\override NoteHead.stencil = \tristencil   } 

triangolodownonce = {
  \once \override Dots.dot-count = #0 %https://lilypond.org/doc/v2.21/Documentation/notation/writing-rhythms
\once \hide Stem
\once\override NoteHead #'no-ledgers = ##t
  \once\override Accidental #'stencil = ##f
  \once\override NoteHead.stencil = \tristencildown   } 


terzavoce = \relative c''
{ 
   \override Staff.StaffSymbol.line-count = #1 %a che serve? 
   R1*7/4 R1*7/4 R1*7/4
   <<{\triangolouponce    b1.\-^\frfl} {s16*22 \triangolodownonce b8}>>
      % s1\-^\markup{ \triangoloup
         %\translate #'(0 . 0) 
         %\triangle ##t  
       %}
       
  
}   
   

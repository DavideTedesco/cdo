ssv = \markup{sempre senza vibrare}
sub = \markup{\huge subarmonico (distorsione)} %grandezza caratteri https://lilypond.org/doc/v2.23/Documentation/notation/formatting-text
subgli = \markup{\huge subarmonico (glissando)}
senint = \markup{\huge {senza intonare}
                          }
subario = \markup{\huge {\column{
                                          \line{Subarmonico}
                                          \line{arioso}
                                          \line{irregolare}
}}}
cdifon = \markup{\huge "Canto difonico"}

%======XNOTE CIRCLE
 xCircleOn = {
 \override NoteHead.stencil = #ly:text-interface::print
  \override NoteHead.text = #(markup #:musicglyph "noteheads.s2xcircle" ) 
}

xCircleOnce = {
 \once \override NoteHead.stencil = #ly:text-interface::print
 \once \override NoteHead.text = #(markup #:musicglyph "noteheads.s2xcircle" ) 
}

xCircleOff = {
  \revert NoteHead.stencil
}

sottov = \markup{\huge Parlato sottovoce}
%===========TEXTSPAN
ttspan = { \override TextSpanner.style = #'line 
                \override TextSpanner.thickness = #3
                 \override TextSpanner.Y-offset = #-5
                 \override TextSpanner.bound-details.right.text =
    \markup { \draw-line #'(0 . 1) }} %https://lsr.di.unimi.it/LSR/Item?id=271
    
%======MUSIC
secondavoce = \relative c
{ 
              R1*7/4  
              r1. fis4^\ssv\mf 
              (fis1)(fis4) r4 r4 
              <f f,\harmonic>1.^\sub
              r1 r4 <<{gis4 
              (gis1)} {s4\f\> s4 s4 s4  s4\p\!}>> \breathe  \xCircleOn c,2\f^\senint^\footnote #'(1 . 1) "Senza intonare, pronunciando solo il rumore" %https://lilypond.org/doc/v2.23/Documentation/notation/creating-footnotes   
              (c2) \xCircleOff <g' g,\harmonic>1^\sub_~ %inserire freccia da lu a [ɯ] per passaggio graduale
              <g g,\harmonic>2._\markup{[ɯ]} r4 
              r1 
              cis,2 e2\p\< 
              (e2) << {a2 
              (a1 
              a2) } {s4\f\> s4 s4 s4 s4 s4 s4 s4\p} >> \breathe r2  
              r2 <<{<fis fis,\harmonic>2^\sub 
              (<fis fis,\harmonic>1)} {s4\p\< s4 s4 s4 s4 s4\ff} >> 
              r1
              <<{<g g,\harmonic>2^\subgli\glissando} {s4\mf s4 } >> 
              <<{<fis fis,\harmonic>1.} {s4\> s4 s4 s4 s4 s4\p}>>
              \xCircleOnce  c2  r4 r4 
                  <<{gis'2} {s4\mf\> s4}>>
              <<{gis2.} { s4 s4 s4\pp}>> \breathe <<{g2.\glissando^\rallentando
              ais1.} {s4\< s4 s4 s4 s4 s4 s4\ff}>> 
              
              \once \hideNotes c1.^\markup{\column{
                                                \line{\huge { Parlato} }
                                                \line{didascalico, neutro}
                                                \line{rassegnato, solido, ironico}
                                                \line{introducendo un}
                                                \line{soggetto al pubblico}
                                }}^\markup{\circle "4" \bold{\teeny {sulla fine della parola}}}%da capire dove metterlo
              R1*6/4
              %\pageBreak
              \xCircleOn 
              \alniente
              c,2..^\sottov\mf\> c8 (c2)
              <<{c2.} {s4 s4 s4\!}>> r2.
              \xCircleOff
              \clef bass
              <f f,\harmonic>1^\sub
              (<f f,\harmonic>1)
             ( <f f,\harmonic>2) r2
             %fine di A
             r1^\markup{\box{\column{
                                                       \line{Fa uscire la mano}
                                                       \line{ sinistra dal telo}
                                                       \line{inseme al crescendo}
                                                       \line{del flauto (lentamente)}
             
             }}}
             r1^\markup{\italic {inserire disegno mano sx fuori dal telo a destra}}%modificare
             r1^\rallentando
             r1^\markup{\box{\column{
                                              \line{La mano viene}
                                                      \line{ritirata lentamente} 
                                                      \line{dentro il pannello}
                        }
             }}
             r1
             r1
             r1
             r4 <f f,\harmonic>2.^\sub^\markup{\circle "5"}\mf
             (<f f,\harmonic>1) \breathe
             <fis fis,\harmonic>1\f \breathe
             <fis fis,\harmonic>1\p \breathe \alniente <e e,\harmonic>2\<^\subario
            (<<{<f f,\harmonic>1.} { \alniente s4\mf\> s4 s4 s4 s4  s4\!}>>)
            r1.^\cdifon
              \ttspan
            <<{c1}{\alniente s4\startTextSpan\<  s4_\markup{2} s4 s4\mf \stopTextSpan}>> r8   <<{d4.
            (d1.)}{\alniente s4\startTextSpan\<_\markup{2}  s8   s4_\markup{6} s4 s4 s4 s4_\markup{8} s4\f \stopTextSpan}>> 
            <<{dis1.\f 
               (dis2)} {s8_\markup{10}\startTextSpan s8*5  s8_\markup{8} s8*4 s8*4 s8\stopTextSpan}>>   \slurDotted \acciaccatura bes8 r8 \slurSolid
           \alniente <<{dis2. (dis8) (dis1) (dis4)} { s8_\markup{10}\< \startTextSpan s8 s8 s8 s8 s8  s8  s8_\markup{6}\f s8*3  s8_\markup{8} s8*3 s8\stopTextSpan}>> r4
           r1 r8
            
} 
\addlyrics {
  [a] %https://www.ipachart.com/
  [o] lu   [∫] lu lu  cen te [ɐ]--------------------[a] [a] -  [∫] [ɐ]---[a] -    vi-[i] [ɪ]  esseri e- sse-  ri vi-----bran-----ti
  [a] [a]
}
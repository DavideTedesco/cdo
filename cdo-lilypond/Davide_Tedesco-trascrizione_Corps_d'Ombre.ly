%[1]   VERSIONE ==============
\version "2.20.0"

%[2]  BLOCCO PAPER==================================
\include "template/papera4P_with_page_number.ly"

%[3]  BLOCCO HEADER==================================
 \header { 
                 title = "Corps d'Ombre"
                 composer = "Davide Tedesco"
                 subsubtitle = "per Flauto Basso, Voce Maschile e Live Electronics"
                 %tagline = "2023"
                 %dedication = "ad Elena D'Alò ed Emanuele Gizzi"
 }

%[4]  BLOCCO NOTAZIONE==================================

\include "personalizzazioni.ly"

\include "righi/flautobasso.ly"
\include "righi/voce.ly"
\include "righi/elettronica_palco.ly"
\include "righi/elettronica_sala.ly"

%\include "voci/tempo.ly"


%[5]  BLOCCO IMPOSTAZIONE RIGHI==========================
\include "template/FlautoBasso_Voce_Maschile_template.ly"


%[6]  BLOCCO SCORE==================================
       \score {

%[6a]  parti da includere========================


       %\rigozero
       \new ChoirStaff<<
        \primorigo
	\secondorigo
	\terzorigo %DA SCOMMENTARE PER ELETTRONICA
	\quartorigo
       >>
      
	


%[6b] PERSONALIZZAZIONE GRAFICA==========
 \layout {
            %indent = -1
    \context {
      \Voice
            \consists "Duration_line_engraver" %https://lilypond.org/doc/v2.23/Documentation/notation/graphical-notation.it.html
            %\override NoteHead.duration-log = 1 %descrive la lungheza di ogni linea
              \override DurationLine.bound-details.right.padding = -2 %sovrappone la linea a quella successiva
    }
    \context {
            %\Staff \RemoveEmptyStaves
      %\override VerticalAxisGroup.remove-first = ##t
      
      \Score
       \accidentalStyle choral-cautionary
      \override DynamicText.direction = #UP
      %per spostare le indicazioni dinamiche sopra la partitura
      \override DynamicLineSpanner.direction = #UP
      \override Flag.stencil = #modern-straight-flag
      
      \override Glissando.thickness = #10/3
      % \hide SpanBar 
      %++++++++tweaks for propotional notation
      proportionalNotationDuration = #(ly:make-moment 1/8)
      \override SpacingSpanner.uniform-stretching = ##f
      \override Score.SpacingSpanner.strict-note-spacing = ##t

      %\override PaperColumn.used = ##t
      %++++++++tweaks for hiding bars, bar numbers and time
      %\hide Staff.BarLine
      %\hide TimeSignature
      %\omit BarNumber    
            \override TimeSignature.font-size = #6

     

    }
    %\context {
     % \Voice
     % \override HorizontalBracket.direction = #UP
      %\override HorizontalBracket.bracket-flare = #'(0 . 0)
      %\override HorizontalBracket.edge-height  = #'(1 . 1)
      %\override HorizontalBracket.shorten-pair =#'(0 . -2.5)
      %\consists "Horizontal_bracket_engraver"
      

    %}
  }
%[6c] OUTPUT MIDI===================
} 

%[7] OPTIONS FOR EXPORT===================
%disable hyperlinks
%\pointAndClickOff
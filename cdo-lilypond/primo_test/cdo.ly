\version "2.22.2"

global = {
  \key c \major
  \time 4/4
}

flute = \relative c'' {
  \global
  % Qui segue la musica.
  c c
  
}

bassVoice = \relative c {
  \global
  \dynamicUp
  % Qui segue la musica.
  c c
  
}

verse = \lyricmode {
  % Qui seguono le parole.
  
}

flutePart = \new Staff \with {
  instrumentName = "Flauto"
  midiInstrument = "flute"
} \flute

bassVoicePart = \new Staff \with {
  instrumentName = "Basso"
  midiInstrument = "choir aahs"
} { \clef bass \bassVoice }
\addlyrics { \verse }

\score {
  <<
    \flutePart
    \bassVoicePart
  >>
  \layout { }
  \midi {
    \tempo 4=100
  }
}

*A*

Ombra, ner**a**

anima lucente

Orma, nera, fragranza vissuta, ombra sbiadita, esseri vibranti

*E*

Candida luc**e**

che fa specchiar l’anima saggia

nel ghiaccio, flora sempreverde che pur trema

*I*

Sangue fis**i**co

Bocca gaia

nella collera o nelle ebrezze penitenti

*U*

Tondi e smeraldei l****u****oghi di pace

fauna vuota di uomini incauti

ricca di quiete che dona

riflessione ed alchimia agli studiosi

*O*

Ora che l’ombra

rinnega il suo corpo

Come corpi, come c**o**r (corps) (corpse),

come corpo

come ombra, come corpo d’ombra

Stridori e silenzi di Univers**o** ultraterren**o** ed uman**o**

facciano congiungere l’Omega per mezzo del viola fluss**o**

al suo sguardo

*Epilogo*

Corp**o** v**u**ot**o**

**u**n**i**one di sangu**e**

legam**e** di **a**nima ed ombr**a**

Corp**o** sperso nei g**o**lfi d’**o**mbr**a**

Vi**a**ggio iridato c**u**stod**e** del passagg**i**o del temp**o**
